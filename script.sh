#!/bin/bash

echo "Atualizando o servidor..."

sudo apt-get update
sudo apt-get upgrade -y

echo "Instalando o apache..."
sudo apt-get install apache2 -y
systemctl status apache2

echo "Baixando os arquivos da aplicação..."
sudo git clone https://github.com/denilsonbonatti/mundo-invertido.git
cd mundo-invertido
sudo cp * -R /var/www/html

echo "Verificando o ip..."
ip a